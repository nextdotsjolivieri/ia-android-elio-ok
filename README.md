# Android App - Cinepolis API:
Project used to work with Cinepolis API 

## Instructions:

### Run the project:  
1. Clone the repo:  
   `git clone https://eliosf27@bitbucket.org/nextdotsjolivieri/ia-test-android-elio.git`  
2. Enter in the directory:  
   `cd ia-test-android-elio/`     
3. Open the project with Android Studio and build gradle:   
4. Thats it!!! Congratulations

**Technologies:**  
   Android SDK, MVP Clean Architecture, Retrofit, Dagger, EventBus, ButterKnife, YoYo Animations, SQLite database, Material Design
