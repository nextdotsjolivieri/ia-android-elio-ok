package com.nextdots.elio.nextdots.base.entities;

import java.io.Serializable;

public class Trailer implements Serializable {
    public int Id;
    public int IdPelicula;
    public String Archivo;
    public String Tipo;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getIdPelicula() {
        return IdPelicula;
    }

    public void setIdPelicula(int idPelicula) {
        IdPelicula = idPelicula;
    }

    public String getArchivo() {
        return Archivo;
    }

    public void setArchivo(String archivo) {
        Archivo = archivo;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }
}
