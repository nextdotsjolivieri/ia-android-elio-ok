package com.nextdots.elio.nextdots.base.events;

import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.base.entities.City;
import com.nextdots.elio.nextdots.base.entities.Place;

import java.util.List;

public class BaseEvent {
    public final static int onListCitiesSuccess = 1;
    public final static int onListPlacesByCityIdSuccess = 2;
    public final static int onListBillboardByPlaceIdSuccess = 3;
    public final static int onListBillboardsSuccess = 4;
    public final static int onListGalleryImagesSuccess = 5;
    public final static int onError = 6;

    private List<City> cities;
    private List<Place> places;
    private List<Billboard> billboards;
    private List<String> images;
    private int type;
    private String error;

    public BaseEvent() {
        this.error = null;
        this.type = 0;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public static int getOnError() {
        return onError;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    public List<Billboard> getBillboards() {
        return billboards;
    }

    public void setBillboards(List<Billboard> billboards) {
        this.billboards = billboards;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}