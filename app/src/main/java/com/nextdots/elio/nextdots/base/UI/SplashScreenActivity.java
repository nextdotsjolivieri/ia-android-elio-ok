package com.nextdots.elio.nextdots.base.UI;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Window;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.domain.ParserDbUtil;
import com.nextdots.elio.nextdots.domain.SharePreferencesUtil;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;

public class SplashScreenActivity extends Activity {

    // Set the duration of the splash screen
    private static final long SPLASH_SCREEN_DELAY = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.splash_screen);
        ButterKnife.bind(this);
        YoYo.with(Techniques.FadeInUp)
                .duration(700)
                .playOn(findViewById(R.id.logo));

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (ParserDbUtil.isDatabaseAlreadyDownloaded(SplashScreenActivity.this)) {
                    SharePreferencesUtil sharePreferencesUtil = new SharePreferencesUtil(SplashScreenActivity.this);
                    Bundle args = new Bundle();
                    args.putString(Config.KEY_CITY, sharePreferencesUtil.getCityId());
                    startActivity(MainActivity.getCallingIntent(SplashScreenActivity.this, args));
                } else {
                    startActivity(CitiesActivity.getCallingIntent(SplashScreenActivity.this));
                }

                // Close the activity so the UserApi won't able to go back this
                // activity pressing Back button
                finish();
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }
}