package com.nextdots.elio.nextdots.base;


public interface BaseInteractor {
    void onListCities();

    void onListPlacesByCitiId(String cityId);

    void onListBillboardByPlaceId(String placeId);

    void onListBillboards(String movieId);

    void onListGalleryImages(String movieId);
}