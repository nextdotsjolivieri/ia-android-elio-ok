package com.nextdots.elio.nextdots;

import android.app.Application;
import android.content.Context;

import com.nextdots.elio.nextdots.base.DI.BaseComponent;
import com.nextdots.elio.nextdots.base.DI.BaseModule;
import com.nextdots.elio.nextdots.base.DI.CityModule;
import com.nextdots.elio.nextdots.base.DI.DaggerBaseComponent;
import com.nextdots.elio.nextdots.base.DI.TabsModule;
import com.nextdots.elio.nextdots.base.UI.BaseView;
import com.nextdots.elio.nextdots.base.UI.CityView;
import com.nextdots.elio.nextdots.base.UI.TabsView;
import com.nextdots.elio.nextdots.lib.DI.LibsModule;

//import com.nextdots.elio.nextdots.base.DI.DaggerBaseComponent;

public class CinepolisApp extends Application {

    public BaseComponent getBaseComponent(Context context, CityView cityView) {
        return DaggerBaseComponent
                .builder()
                .libsModule(new LibsModule())
                .baseModule(new BaseModule(null))
                .cityModule(new CityModule(context, cityView))
                .tabsModule(new TabsModule(context, null))
                .build();
    }

    public BaseComponent getBaseComponent(Context context, BaseView baseView) {
        return DaggerBaseComponent
                .builder()
                .libsModule(new LibsModule())
                .baseModule(new BaseModule(baseView))
                .cityModule(new CityModule(context, null))
                .tabsModule(new TabsModule(context, null))
                .build();
    }

    public BaseComponent getBaseComponent(Context context, TabsView tabsView) {
        return DaggerBaseComponent
                .builder()
                .libsModule(new LibsModule())
                .baseModule(new BaseModule(null))
                .cityModule(new CityModule(context, null))
                .tabsModule(new TabsModule(context, tabsView))
                .build();
    }
}
