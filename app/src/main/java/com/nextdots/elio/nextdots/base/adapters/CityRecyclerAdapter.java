package com.nextdots.elio.nextdots.base.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.entities.City;

import java.util.List;

public class CityRecyclerAdapter extends RecyclerView.Adapter<CityRecyclerAdapter.CityViewHolder> {

    private List<City> cities;
    private FragmentActivity context;

    public CityRecyclerAdapter(FragmentActivity context, @Nullable List<City> cities) {
        this.cities = cities;
        this.context = context;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflatedView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_city, viewGroup, false);
        return new CityViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        City product = this.cities.get(position);
        holder.txtName.setText(product.getNombre());
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public City getItem(int index) {
        return cities.get(index);
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;

        CityViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
        }
    }
}
