package com.nextdots.elio.nextdots.base.UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.nextdots.elio.nextdots.CinepolisApp;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.CityPresenter;
import com.nextdots.elio.nextdots.base.DI.BaseComponent;
import com.nextdots.elio.nextdots.base.adapters.CityRecyclerAdapter;
import com.nextdots.elio.nextdots.base.entities.City;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.domain.FragmentUtils;
import com.nextdots.elio.nextdots.domain.RecyclerItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CitiesActivity extends AppCompatActivity implements CityView, RecyclerItemClickListener.OnItemClickListener {

    private CityPresenter presenter;
    @BindView(R.id.citiesRecyclerView)
    RecyclerView citiesRecyclerView;
    private ProgressDialog progressDialog;
    private CityRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities);
        ButterKnife.bind(this);

        setupInjection();

        citiesRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CitiesActivity.this);
        citiesRecyclerView.setLayoutManager(linearLayoutManager);
        citiesRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(CitiesActivity.this, this)
        );

        presenter.onCreate();
        presenter.onListCities();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CitiesActivity.this);
            progressDialog.setMessage(getString(R.string.loading_cities));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(CitiesActivity.this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onListCities(List<City> cities) {
        if (cities != null) {
            adapter = new CityRecyclerAdapter(CitiesActivity.this, cities);
            citiesRecyclerView.setAdapter(adapter);
        }
    }

    public void setupInjection() {
        CinepolisApp app = new CinepolisApp();
        BaseComponent productComponent = app.getBaseComponent(CitiesActivity.this, this);
        presenter = productComponent.getCityPresenter();
    }

    @Override
    public void onItemClick(View childView, int position) {
        final City obj = adapter.getItem(position);
        Bundle args = new Bundle();
        args.putString(Config.KEY_CITY, String.valueOf(obj.getId()));
        startActivity(MainActivity.getCallingIntent(this, args));
    }

    @Override
    public void onItemLongPress(View childView, int position) {
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, CitiesActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }
}