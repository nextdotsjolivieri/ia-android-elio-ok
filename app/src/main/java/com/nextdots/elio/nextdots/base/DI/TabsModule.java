package com.nextdots.elio.nextdots.base.DI;


import android.content.Context;

import com.nextdots.elio.nextdots.base.BaseInteractor;
import com.nextdots.elio.nextdots.base.BaseInteractorImpl;
import com.nextdots.elio.nextdots.base.BaseRepository;
import com.nextdots.elio.nextdots.base.BaseRepositoryImpl;
import com.nextdots.elio.nextdots.base.TabsPresenter;
import com.nextdots.elio.nextdots.base.TabsPresenterImpl;
import com.nextdots.elio.nextdots.base.UI.TabsView;
import com.nextdots.elio.nextdots.lib.base.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TabsModule {
    private TabsView tabsView;
    private Context context;

    public TabsModule(Context context, TabsView tabsView) {
        this.tabsView = tabsView;
        this.context = context;
    }

    @Provides
    @Singleton
    TabsView providesTabsView() {
        return this.tabsView;
    }

    @Provides
    @Singleton
    TabsPresenter providesTabsPresenter(EventBus eventBus,
                                        TabsView tabsView,
                                        BaseInteractor baseInteractor) {
        return new TabsPresenterImpl(eventBus, tabsView, baseInteractor);
    }
}
