package com.nextdots.elio.nextdots.base;


public class BaseInteractorImpl implements BaseInteractor {

    private BaseRepository baseRepository;

    public BaseInteractorImpl(BaseRepository baseRepository) {
        this.baseRepository = baseRepository;
    }

    @Override
    public void onListCities() {
        baseRepository.onListCities();
    }

    @Override
    public void onListPlacesByCitiId(String cityId) {
        baseRepository.onListPlacesByCitiId(cityId);
    }

    @Override
    public void onListBillboardByPlaceId(String placeId) {
        baseRepository.onListBillboardByPlaceId(placeId);
    }

    @Override
    public void onListBillboards(String movieId) {
        baseRepository.onListBillboards(movieId);
    }

    @Override
    public void onListGalleryImages(String movieId) {
        baseRepository.onListGalleryImages(movieId);
    }
}