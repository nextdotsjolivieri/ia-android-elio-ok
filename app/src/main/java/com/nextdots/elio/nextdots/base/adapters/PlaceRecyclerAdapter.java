package com.nextdots.elio.nextdots.base.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.entities.Place;

import java.util.List;

public class PlaceRecyclerAdapter extends RecyclerView.Adapter<PlaceRecyclerAdapter.CityViewHolder> {

    private List<Place> places;
    private FragmentActivity context;

    public PlaceRecyclerAdapter(FragmentActivity context, @Nullable List<Place> places) {
        this.places = places;
        this.context = context;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflatedView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_place, viewGroup, false);
        return new CityViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        Place place = this.places.get(position);
        holder.txtName.setText(place.getNombre());
        holder.txtAddress.setText(place.getDireccion());
    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    public Place getItem(int index) {
        return places.get(index);
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        TextView txtAddress;

        CityViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
        }
    }
}
