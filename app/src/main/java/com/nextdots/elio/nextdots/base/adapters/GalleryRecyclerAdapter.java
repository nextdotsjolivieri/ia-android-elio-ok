package com.nextdots.elio.nextdots.base.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nextdots.elio.nextdots.R;

import java.util.List;

public class GalleryRecyclerAdapter extends RecyclerView.Adapter<GalleryRecyclerAdapter.CityViewHolder> {

    private List<String> images;
    private FragmentActivity context;

    public GalleryRecyclerAdapter(FragmentActivity context, @Nullable List<String> images) {
        this.images = images;
        this.context = context;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflatedView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_gallery, viewGroup, false);
        return new CityViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        String url = this.images.get(position);
        Glide
                .with(context)
                .load(url)
                .centerCrop()
                .crossFade()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public String getItem(int index) {
        return images.get(index);
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        CityViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imgView);
        }
    }
}
