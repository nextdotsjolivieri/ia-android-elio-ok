package com.nextdots.elio.nextdots.domain;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.nextdots.elio.nextdots.R;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;


public class DownloadDatabase extends AsyncTask<Void, Integer, String> {

    private static final String DOWNLOAD_DB_URL = "http://api.cinepolis.com.mx/sqlite.aspx?idCiudad=";
    private ProgressDialog progressDialog;
    private Context context;
    private String cityId;
    private OnPostExcecutedCallback onPostExcecutedCallback;

    public interface OnPostExcecutedCallback {
        void onPostExecuted();
    }

    public void setOnPostExecutedCallback(OnPostExcecutedCallback onPostExcecutedCallback) {
        this.onPostExcecutedCallback = onPostExcecutedCallback;
    }

    public DownloadDatabase(Context context, String cityId) {
        this.context = context;
        this.cityId = cityId;
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.loading_data));
            progressDialog.setIndeterminate(true);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.show();
    }

    @Override
    protected String doInBackground(Void... urlParams) {
        String filename = Config.DATABASE_NAME;
        int count;
        try {
            String urlStr = DOWNLOAD_DB_URL + cityId;
            URL url = new URL(urlStr);
            URLConnection conection = url.openConnection();
            conection.connect();
            // getting file length
            int lenghtOfFile = conection.getContentLength();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);
            FileOutputStream outputStream;
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            byte data[] = new byte[1024];
            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                publishProgress((int) ((total * 100) / lenghtOfFile));
                // writing data to file
                outputStream.write(data, 0, count);
            }

            // flushing output
            outputStream.flush();

            // closing streams
            outputStream.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return filename;
    }

    protected void onProgressUpdate(Integer... prog) {
        this.progressDialog.setProgress(prog[0]);
    }

    protected void onPostExecute(String name) {
        this.progressDialog.dismiss();
        SharePreferencesUtil sharePreferencesUtil = new SharePreferencesUtil(context);
        sharePreferencesUtil.saveCityId(cityId);
        if (onPostExcecutedCallback != null) {
            onPostExcecutedCallback.onPostExecuted();
        }
    }
}