package com.nextdots.elio.nextdots.base.UI;

import com.nextdots.elio.nextdots.base.entities.City;

import java.util.List;

public interface CityView {
    void showProgress();

    void hideProgress();

    void onError(String error);

    void onListCities(List<City> cities);
}
