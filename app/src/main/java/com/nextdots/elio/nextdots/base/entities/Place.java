package com.nextdots.elio.nextdots.base.entities;

import java.io.Serializable;

public class Place implements Serializable {
    public int Id;
    public String IdComplejoVista;
    public int IdCiudad;
    public String Nombre;
    public String Latitud;
    public String Longitud;
    public String Telefono;
    public String Direccion;
    public boolean EsCompraAnticipada;
    public boolean EsReservaPermitida;
    public boolean EsVentaAlimentos;
    public String UrlSitio;
    public String Transporte;
    public String MerchId;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getIdComplejoVista() {
        return IdComplejoVista;
    }

    public void setIdComplejoVista(String idComplejoVista) {
        IdComplejoVista = idComplejoVista;
    }

    public int getIdCiudad() {
        return IdCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        IdCiudad = idCiudad;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String latitud) {
        Latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        Longitud = longitud;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public boolean isEsCompraAnticipada() {
        return EsCompraAnticipada;
    }

    public void setEsCompraAnticipada(boolean esCompraAnticipada) {
        EsCompraAnticipada = esCompraAnticipada;
    }

    public boolean isEsReservaPermitida() {
        return EsReservaPermitida;
    }

    public void setEsReservaPermitida(boolean esReservaPermitida) {
        EsReservaPermitida = esReservaPermitida;
    }

    public String getUrlSitio() {
        return UrlSitio;
    }

    public void setUrlSitio(String urlSitio) {
        UrlSitio = urlSitio;
    }

    public String getTransporte() {
        return Transporte;
    }

    public void setTransporte(String transporte) {
        Transporte = transporte;
    }

    public String getMerchId() {
        return MerchId;
    }

    public void setMerchId(String merchId) {
        MerchId = merchId;
    }

    public boolean isEsVentaAlimentos() {
        return EsVentaAlimentos;
    }

    public void setEsVentaAlimentos(boolean esVentaAlimentos) {
        EsVentaAlimentos = esVentaAlimentos;
    }
}
