package com.nextdots.elio.nextdots.base.UI;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.domain.Config;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TrailerTabFragment extends Fragment {

    @BindView(R.id.VideoView)
    VideoView videoview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trailer, container, false);
        ButterKnife.bind(this, view);

        Bundle args = getArguments();
        if (args != null) {
            Billboard billboard = (Billboard) args.getSerializable(Config.KEY_BILLBOARD);
            if (billboard != null) {
                playVideoTrailer(String.valueOf(billboard.getPelicula().getId()));
            }
        }

        return view;
    }

    public static TrailerTabFragment newInstance(Bundle args) {
        TrailerTabFragment fragment = new TrailerTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void playVideoTrailer(String movieId) {
        try {
            MediaController mediacontroller = new MediaController(getActivity());
            mediacontroller.setAnchorView(videoview);
            String videoURL = Config.API_VIDEO_URL + movieId + "/1/2/" + movieId + ".webm";
            Uri video = Uri.parse(videoURL);
            videoview.setMediaController(mediacontroller);
            videoview.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoview.requestFocus();
        videoview.setOnPreparedListener(mp -> videoview.start());
    }
}