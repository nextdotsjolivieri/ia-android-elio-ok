package com.nextdots.elio.nextdots.base.DI;


import android.content.Context;

import com.nextdots.elio.nextdots.base.BaseInteractor;
import com.nextdots.elio.nextdots.base.BaseInteractorImpl;
import com.nextdots.elio.nextdots.base.CityPresenter;
import com.nextdots.elio.nextdots.base.CityPresenterImpl;
import com.nextdots.elio.nextdots.base.BaseRepository;
import com.nextdots.elio.nextdots.base.BaseRepositoryImpl;
import com.nextdots.elio.nextdots.base.UI.CityView;
import com.nextdots.elio.nextdots.lib.base.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CityModule {
    private CityView cityView;
    private Context context;

    public CityModule(Context context, CityView cityView) {
        this.cityView = cityView;
        this.context = context;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return this.context;
    }

    @Provides
    @Singleton
    CityView providesCityView() {
        return this.cityView;
    }

    @Provides
    @Singleton
    CityPresenter providesCityPresenter(EventBus eventBus,
                                        CityView cityView,
                                        BaseInteractor baseInteractor) {
        return new CityPresenterImpl(eventBus, cityView, baseInteractor);
    }

    @Provides
    @Singleton
    BaseInteractor providesCityInteractor(BaseRepository baseRepository) {
        return new BaseInteractorImpl(baseRepository);
    }

    @Provides
    @Singleton
    BaseRepository providesCityRepository(Context context, EventBus eventBus) {
        return new BaseRepositoryImpl(context, eventBus);
    }
}
