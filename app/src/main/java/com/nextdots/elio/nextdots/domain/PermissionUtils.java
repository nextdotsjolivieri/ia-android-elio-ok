package com.nextdots.elio.nextdots.domain;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


public class PermissionUtils {

    public interface PermissionGrantedCallBack {
        void onPermissionGranted();
    }

    public static void loadPermissions(
            Activity context, String[] perms,
            int requestCode, PermissionGrantedCallBack callBack) {
        for (String perm : perms) {
            if (ContextCompat.checkSelfPermission(context, perm) != PackageManager.PERMISSION_GRANTED) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(context, perm)) {
                    ActivityCompat.requestPermissions(context, new String[]{perm}, requestCode);
                }
            } else {
                if (callBack != null) {
                    callBack.onPermissionGranted();
                }
            }
        }

    }
}
