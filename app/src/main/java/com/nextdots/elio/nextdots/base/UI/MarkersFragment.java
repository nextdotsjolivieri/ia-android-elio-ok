package com.nextdots.elio.nextdots.base.UI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nextdots.elio.nextdots.CinepolisApp;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.BasePresenter;
import com.nextdots.elio.nextdots.base.DI.BaseComponent;
import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.base.entities.Place;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.domain.FragmentUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class MarkersFragment extends Fragment implements OnMapReadyCallback, BaseView {

    private ProgressDialog progressDialog;
    private GoogleMap mMap;
    private BasePresenter presenter;
    private MapView mapView;
    private List<Place> places = new ArrayList<>();

    public MarkersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_marker_maps, container, false);

        ButterKnife.bind(this, view);

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        setupInjection();

        if (getArguments() != null && mMap != null) {
            String cityId = getArguments().getString(Config.KEY_CITY);
            presenter.onCreate();
            presenter.onListPlaces(cityId);
        }

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(marker -> {
            for (Place place : places) {
                if (place.getNombre().equals(marker.getTitle())) {
                    Fragment fragment = new PlaceDetailsFragment();
                    Bundle args = new Bundle();
                    args.putSerializable(Config.KEY_PLACE, place);
                    FragmentUtils.setFragmentContent(getActivity(), R.id.fragment_content, fragment, args);
                }
            }
        });
        if (getArguments() != null) {
            String cityId = getArguments().getString(Config.KEY_CITY);
            presenter.onCreate();
            presenter.onListPlaces(cityId);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        this.places = new ArrayList<>();
        presenter.onDestroy();
        mapView.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading_places));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onListPlaces(List<Place> places) {
        this.places = places;
        for (Place place : places) {
            LatLng placeSelected = new LatLng(
                    Double.parseDouble(place.getLatitud()),
                    Double.parseDouble(place.getLongitud())
            );
            mMap.addMarker(new MarkerOptions().position(placeSelected).snippet(place.getDireccion()).title(place.getNombre()));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(placeSelected));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        }
    }

    @Override
    public void onListBillboard(List<Billboard> billboards) {
    }

    public void setupInjection() {
        CinepolisApp app = new CinepolisApp();
        BaseComponent productComponent = app.getBaseComponent(getActivity(), this);
        presenter = productComponent.getBasePresenter();
    }
}
