package com.nextdots.elio.nextdots.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.nextdots.elio.nextdots.domain.Config.API_URL;


public class ApiClient {
    public static Retrofit retrofit() {
        OkHttpClient.Builder builderClient = new OkHttpClient().newBuilder();
        OkHttpClient okHttpClient = builderClient
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}