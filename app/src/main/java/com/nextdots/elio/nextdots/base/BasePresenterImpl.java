package com.nextdots.elio.nextdots.base;


import com.nextdots.elio.nextdots.base.UI.BaseView;
import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.base.entities.Place;
import com.nextdots.elio.nextdots.base.events.BaseEvent;
import com.nextdots.elio.nextdots.lib.base.EventBus;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class BasePresenterImpl implements BasePresenter {

    private EventBus eventBus;
    private BaseView baseView;
    private BaseInteractor baseInteractor;

    public BasePresenterImpl(EventBus eventBus, BaseView baseView, BaseInteractor baseInteractor) {
        this.eventBus = eventBus;
        this.baseView = baseView;
        this.baseInteractor = baseInteractor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        baseView = null;
        eventBus.unregister(this);
    }

    @Override
    public void onListPlaces(String cityId) {
        baseView.showProgress();
        baseInteractor.onListPlacesByCitiId(cityId);
    }

    @Override
    public void onListBillboardByPlaceId(String placeId) {
        baseView.showProgress();
        baseInteractor.onListBillboardByPlaceId(placeId);
    }

    @Override
    @Subscribe
    public void onEventMainThread(BaseEvent event) {
        switch (event.getType()) {
            case BaseEvent.onListPlacesByCityIdSuccess:
                onListPlacesByCityIdSuccess(event.getPlaces());
                break;
            case BaseEvent.onListBillboardByPlaceIdSuccess:
                onListBillboardByPlaceIdSuccess(event.getBillboards());
                break;
            case BaseEvent.onError:
                onDownloadError(event.getError());
                break;
        }
    }

    private void onDownloadError(String error) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.onError(error);
        }
    }

    private void onListPlacesByCityIdSuccess(List<Place> places) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.onListPlaces(places);
        }
    }

    private void onListBillboardByPlaceIdSuccess(List<Billboard> billboards) {
        if (baseView != null) {
            baseView.hideProgress();
            baseView.onListBillboard(billboards);
        }
    }

    @Override
    public BaseView getView() {
        return baseView;
    }
}
