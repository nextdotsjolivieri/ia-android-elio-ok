package com.nextdots.elio.nextdots.base.UI;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.adapters.SectionsPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BillboardDetailsFragment extends Fragment {

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_billboard_details, container, false);
        ButterKnife.bind(this, view);

        setupViewPager(pager);
        tabLayout.setupWithViewPager(pager);

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(GalleryTabFragment.newInstance(getArguments()), getString(R.string.tab_title_gallery));
        adapter.addFragment(TrailerTabFragment.newInstance(getArguments()), getString(R.string.tab_title_trailer));
        adapter.addFragment(SchedulesTabFragment.newInstance(getArguments()), getString(R.string.tab_title_schedules));
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);
    }
}