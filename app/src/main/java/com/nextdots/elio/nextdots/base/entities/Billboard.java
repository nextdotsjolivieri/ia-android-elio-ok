package com.nextdots.elio.nextdots.base.entities;

import java.io.Serializable;

public class Billboard implements Serializable {
    public Movie Pelicula;
    public int IdComplejoVista;
    public int IdComplejo;
    public int IdPeliculaVista;
    public int IdShowtime;
    public String Horario;
    public String Fecha;
    public String Sello;
    public int Sala;
    public int Id;

    public Movie getPelicula() {
        return Pelicula;
    }

    public void setPelicula(Movie pelicula) {
        Pelicula = pelicula;
    }

    public int getIdComplejoVista() {
        return IdComplejoVista;
    }

    public void setIdComplejoVista(int idComplejoVista) {
        IdComplejoVista = idComplejoVista;
    }

    public int getIdComplejo() {
        return IdComplejo;
    }

    public void setIdComplejo(int idComplejo) {
        IdComplejo = idComplejo;
    }

    public int getIdPeliculaVista() {
        return IdPeliculaVista;
    }

    public void setIdPeliculaVista(int idPeliculaVista) {
        IdPeliculaVista = idPeliculaVista;
    }

    public int getIdShowtime() {
        return IdShowtime;
    }

    public void setIdShowtime(int idShowtime) {
        IdShowtime = idShowtime;
    }

    public String getHorario() {
        return Horario;
    }

    public void setHorario(String horario) {
        Horario = horario;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public String getSello() {
        return Sello;
    }

    public void setSello(String sello) {
        Sello = sello;
    }

    public int getSala() {
        return Sala;
    }

    public void setSala(int sala) {
        Sala = sala;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
