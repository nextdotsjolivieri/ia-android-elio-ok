package com.nextdots.elio.nextdots.base.UI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nextdots.elio.nextdots.CinepolisApp;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.DI.BaseComponent;
import com.nextdots.elio.nextdots.base.TabsPresenter;
import com.nextdots.elio.nextdots.base.adapters.BillboardRecyclerAdapter;
import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.domain.Config;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SchedulesTabFragment extends Fragment implements TabsView {

    private TabsPresenter presenter;
    private ProgressDialog progressDialog;
    @BindView(R.id.billboardsScheduleRecyclerView)
    RecyclerView billboardsScheduleRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedules, container, false);
        ButterKnife.bind(this, view);

        setupInjection();

        presenter.onCreate();

        Bundle args = getArguments();
        if (args != null) {
            Billboard billboard = (Billboard) args.getSerializable(Config.KEY_BILLBOARD);
            if (billboard != null) {
                presenter.onListBillboards(String.valueOf(billboard.getPelicula().getId()));
            }
        }

        billboardsScheduleRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        billboardsScheduleRecyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading_cities));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onListBillboards(List<Billboard> billboards) {
        if (billboards != null) {
            BillboardRecyclerAdapter adapter = new BillboardRecyclerAdapter(getActivity(), billboards, true);
            billboardsScheduleRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onListGalleryImages(List<String> images) {
    }

    public void setupInjection() {
        CinepolisApp app = new CinepolisApp();
        BaseComponent productComponent = app.getBaseComponent(getActivity(), this);
        presenter = productComponent.getTabsPresenter();
    }

    public static SchedulesTabFragment newInstance(Bundle args) {
        SchedulesTabFragment fragment = new SchedulesTabFragment();
        fragment.setArguments(args);
        return fragment;
    }
}