package com.nextdots.elio.nextdots.base;


import com.nextdots.elio.nextdots.base.UI.CityView;
import com.nextdots.elio.nextdots.base.entities.City;
import com.nextdots.elio.nextdots.base.events.BaseEvent;
import com.nextdots.elio.nextdots.lib.base.EventBus;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class CityPresenterImpl implements CityPresenter {

    private EventBus eventBus;
    private CityView cityView;
    private BaseInteractor baseInteractor;

    public CityPresenterImpl(EventBus eventBus, CityView cityView, BaseInteractor baseInteractor) {
        this.eventBus = eventBus;
        this.cityView = cityView;
        this.baseInteractor = baseInteractor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        cityView = null;
        eventBus.unregister(this);
    }

    @Override
    public void onListCities() {
        cityView.showProgress();
        baseInteractor.onListCities();
    }

    @Override
    @Subscribe
    public void onEventMainThread(BaseEvent event) {
        switch (event.getType()) {
            case BaseEvent.onListCitiesSuccess:
                onListCategoriesSuccess(event.getCities());
                break;
            case BaseEvent.onError:
                onDownloadError(event.getError());
                break;
        }
    }

    private void onDownloadError(String error) {
        if (cityView != null) {
            cityView.hideProgress();
            cityView.onError(error);
        }
    }

    private void onListCategoriesSuccess(List<City> cities) {
        if (cityView != null) {
            cityView.hideProgress();
            cityView.onListCities(cities);
        }
    }

    @Override
    public CityView getView() {
        return cityView;
    }
}
