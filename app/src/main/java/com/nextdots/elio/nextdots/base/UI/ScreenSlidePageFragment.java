package com.nextdots.elio.nextdots.base.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.domain.TouchImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScreenSlidePageFragment extends Fragment {

    @BindView(R.id.imgView)
    TouchImageView imgView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_slide_page, container, false);

        ButterKnife.bind(this, rootView);

        Bundle args = getArguments();
        if (args != null) {
            String url = args.getString(Config.API_IMAGE_URL);
            Glide
                    .with(getActivity())
                    .load(url)
                    .centerCrop()
                    .crossFade()
                    .into(imgView);
        }

        return rootView;
    }

    public static ScreenSlidePageFragment newInstance(Bundle args) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        fragment.setArguments(args);
        return fragment;
    }
}

