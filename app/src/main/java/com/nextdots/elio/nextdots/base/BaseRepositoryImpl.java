package com.nextdots.elio.nextdots.base;


import android.content.Context;

import com.nextdots.elio.nextdots.api.ApiClient;
import com.nextdots.elio.nextdots.api.services.CityApi;
import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.base.entities.City;
import com.nextdots.elio.nextdots.base.entities.Place;
import com.nextdots.elio.nextdots.base.events.BaseEvent;
import com.nextdots.elio.nextdots.domain.DownloadDatabase;
import com.nextdots.elio.nextdots.domain.ParserDbUtil;
import com.nextdots.elio.nextdots.lib.base.EventBus;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BaseRepositoryImpl implements BaseRepository {
    private EventBus eventBus;
    private Retrofit retrofit;
    private Context context;

    public BaseRepositoryImpl(Context context, EventBus eventBus) {
        this.eventBus = eventBus;
        this.context = context;
        this.retrofit = ApiClient.retrofit();
    }

    @Override
    public void onListCities() {
        CityApi service = retrofit.create(CityApi.class);
        service.cities().enqueue(new Callback<List<City>>() {
            @Override
            public void onResponse(Call<List<City>> call, Response<List<City>> response) {
                if (response.code() == 200) {
                    onListCitiesSuccess(response.body());
                } else {
                    try {
                        onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<City>> call, Throwable t) {
                onError(t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onListPlacesByCitiId(String cityId) {
        if (ParserDbUtil.isDatabaseAlreadyDownloaded(context)) {
            List<Place> placeList = ParserDbUtil.getPlacesByCityId(context, cityId);
            onListPlacesByCitiIdSuccess(placeList);
        } else {
            DownloadDatabase downloadDatabase = new DownloadDatabase(context, cityId);
            downloadDatabase.setOnPostExecutedCallback(() -> {
                List<Place> placeList = ParserDbUtil.getPlacesByCityId(context, cityId);
                onListPlacesByCitiIdSuccess(placeList);
            });
            downloadDatabase.execute();
        }
    }

    @Override
    public void onListBillboardByPlaceId(String placeId) {
        List<Billboard> placeList = ParserDbUtil.getBillboardsByPlaceId(context, placeId);
        onListBillboardByPlaceIdSuccess(placeList);
    }

    @Override
    public void onListBillboards(String movieId) {
        List<Billboard> placeList = ParserDbUtil.getBillboardsByMovieId(context, movieId);
        onListBillboardsSuccess(placeList);
    }

    @Override
    public void onListGalleryImages(String movieId) {
        List<String> imageList = ParserDbUtil.getImagesByMovieId(context, movieId);
        onListGalleryImagesSuccess(imageList);
    }

    private void onListGalleryImagesSuccess(List<String> imageList) {
        onEvent(BaseEvent.onListGalleryImagesSuccess, null, null, null, null, imageList);
    }

    private void onListBillboardsSuccess(List<Billboard> billboards) {
        onEvent(BaseEvent.onListBillboardsSuccess, null, null, null, billboards, null);
    }

    private void onListBillboardByPlaceIdSuccess(List<Billboard> billboards) {
        onEvent(BaseEvent.onListBillboardByPlaceIdSuccess, null, null, null, billboards, null);
    }

    private void onListCitiesSuccess(List<City> cities) {
        onEvent(BaseEvent.onListCitiesSuccess, null, cities, null, null, null);
    }

    private void onListPlacesByCitiIdSuccess(List<Place> places) {
        onEvent(BaseEvent.onListPlacesByCityIdSuccess, null, null, places, null, null);
    }

    private void onError(String error) {
        onEvent(BaseEvent.onError, error, null, null, null, null);
    }

    private void onEvent(int type, String error, List<City> cities,
                         List<Place> places, List<Billboard> billboard,
                         List<String> images) {
        BaseEvent event = new BaseEvent();
        event.setType(type);
        event.setError(error);
        event.setCities(cities);
        event.setPlaces(places);
        event.setBillboards(billboard);
        event.setImages(images);
        eventBus.post(event);
    }


}
