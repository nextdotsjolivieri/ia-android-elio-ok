package com.nextdots.elio.nextdots.base.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nextdots.elio.nextdots.base.UI.ScreenSlidePageFragment;
import com.nextdots.elio.nextdots.domain.Config;

import java.util.ArrayList;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<String> images = new ArrayList<>();

    public ScreenSlidePagerAdapter(FragmentManager fm, ArrayList<String> images) {
        super(fm);
        this.images = images;
    }

    @Override
    public Fragment getItem(int position) {
        String url = images.get(position);
        Bundle args = new Bundle();
        args.putString(Config.API_IMAGE_URL, url);
        return ScreenSlidePageFragment.newInstance(args);
    }

    @Override
    public int getCount() {
        return images.size();
    }
}
