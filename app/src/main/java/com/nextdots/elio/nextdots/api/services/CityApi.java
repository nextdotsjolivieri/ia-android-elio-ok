package com.nextdots.elio.nextdots.api.services;

import com.nextdots.elio.nextdots.base.entities.City;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CityApi {

    @GET("ObtenerCiudades")
    Call<List<City>> cities();
}
