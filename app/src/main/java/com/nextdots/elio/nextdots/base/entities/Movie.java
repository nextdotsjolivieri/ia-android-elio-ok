package com.nextdots.elio.nextdots.base.entities;

import java.io.Serializable;

public class Movie implements Serializable {
    public int Id;
    public String Titulo;
    public String TituloOriginal;
    public String Genero;
    public String Clasificacion;
    public String Duracion;
    public String Director;
    public String Actores;
    public String Sinopsis;
    public String ImagenCartel;
    public String Sello;
    public String SelloEstatus;
    public String Clave;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getTituloOriginal() {
        return TituloOriginal;
    }

    public void setTituloOriginal(String tituloOriginal) {
        TituloOriginal = tituloOriginal;
    }

    public String getGenero() {
        return Genero;
    }

    public void setGenero(String genero) {
        Genero = genero;
    }

    public String getClasificacion() {
        return Clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        Clasificacion = clasificacion;
    }

    public String getDuracion() {
        return Duracion;
    }

    public void setDuracion(String duracion) {
        Duracion = duracion;
    }

    public String getDirector() {
        return Director;
    }

    public void setDirector(String director) {
        Director = director;
    }

    public String getActores() {
        return Actores;
    }

    public void setActores(String actores) {
        Actores = actores;
    }

    public String getSinopsis() {
        return Sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        Sinopsis = sinopsis;
    }

    public String getImagenCartel() {
        return ImagenCartel;
    }

    public void setImagenCartel(String imagenCartel) {
        ImagenCartel = imagenCartel;
    }

    public String getSello() {
        return Sello;
    }

    public void setSello(String sello) {
        Sello = sello;
    }

    public String getSelloEstatus() {
        return SelloEstatus;
    }

    public void setSelloEstatus(String selloEstatus) {
        SelloEstatus = selloEstatus;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String clave) {
        Clave = clave;
    }
}
