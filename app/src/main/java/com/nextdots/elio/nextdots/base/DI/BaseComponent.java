package com.nextdots.elio.nextdots.base.DI;


import com.nextdots.elio.nextdots.base.BasePresenter;
import com.nextdots.elio.nextdots.base.CityPresenter;
import com.nextdots.elio.nextdots.base.TabsPresenter;
import com.nextdots.elio.nextdots.lib.DI.LibsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {CityModule.class, TabsModule.class, BaseModule.class, LibsModule.class})
public interface BaseComponent {
    CityPresenter getCityPresenter();

    BasePresenter getBasePresenter();

    TabsPresenter getTabsPresenter();
}