package com.nextdots.elio.nextdots.base.UI;

import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.base.entities.Place;

import java.util.List;

public interface BaseView {
    void showProgress();

    void hideProgress();

    void onError(String error);

    void onListPlaces(List<Place> places);

    void onListBillboard(List<Billboard> billboards);
}
