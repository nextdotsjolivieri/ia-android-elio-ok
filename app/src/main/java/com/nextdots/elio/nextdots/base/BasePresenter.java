package com.nextdots.elio.nextdots.base;


import com.nextdots.elio.nextdots.base.UI.BaseView;
import com.nextdots.elio.nextdots.base.events.BaseEvent;

public interface BasePresenter {
    void onCreate();

    void onDestroy();

    void onListPlaces(String cityId);

    void onListBillboardByPlaceId(String placeId);

    void onEventMainThread(BaseEvent event);

    BaseView getView();
}
