package com.nextdots.elio.nextdots.domain;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.base.entities.Movie;
import com.nextdots.elio.nextdots.base.entities.Place;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ParserDbUtil {

    private static SQLiteDatabase getSqLiteDatabase(Context context) {
        String name = Config.DATABASE_NAME;
        File file = new File(context.getFilesDir() + "/" + name);
        return SQLiteDatabase.openDatabase(file.getPath(), null, SQLiteDatabase.OPEN_READONLY);
    }

    public static boolean isDatabaseAlreadyDownloaded(Context context) {
        File file = new File(context.getFilesDir() + "/" + Config.DATABASE_NAME);
        return file.exists();
    }

    public static List<Place> getPlacesByCityId(Context context, String cityId) {
        SQLiteDatabase db = getSqLiteDatabase(context);
        List<Place> places = new ArrayList<>();
        if (db != null) {
            String[] args = new String[]{cityId};
            Cursor cursor = db.rawQuery("SELECT * FROM Complejo where IdCiudad=?", args);
            if (cursor.moveToFirst()) {
                do {
                    Place place = new Place();
                    place.setId(cursor.getInt(0));
                    place.setIdComplejoVista(cursor.getString(1));
                    place.setIdCiudad(cursor.getInt(2));
                    place.setNombre(cursor.getString(3));
                    place.setLatitud(cursor.getString(4));
                    place.setLongitud(cursor.getString(5));
                    place.setTelefono(cursor.getString(6));
                    place.setDireccion(cursor.getString(7));
                    place.setEsCompraAnticipada(cursor.getInt(8) == 0);
                    place.setEsReservaPermitida(cursor.getInt(9) == 0);
                    place.setUrlSitio(cursor.getString(10));
                    place.setTransporte(cursor.getString(11));
                    place.setMerchId(cursor.getString(12));
                    place.setEsVentaAlimentos(cursor.getInt(12) == 0);
                    places.add(place);
                } while (cursor.moveToNext());

                cursor.close();
            }
        }
        return places;
    }

    public static List<Billboard> getBillboardsByMovieId(Context context, String movieId) {
        SQLiteDatabase db = getSqLiteDatabase(context);
        List<Billboard> billboards = new ArrayList<>();
        if (db != null) {
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            String[] args = new String[]{movieId, date};
            Cursor cursor = db.rawQuery("SELECT * FROM Cartelera AS c JOIN Pelicula AS p ON c.IdPelicula=p.Id  WHERE c.IdPelicula=? and c.Fecha=?", args);
            if (cursor.moveToFirst()) {
                do {
                    Billboard billboard = new Billboard();
                    Movie movie = new Movie();
                    movie.setId(cursor.getInt(0));
                    movie.setTitulo(cursor.getString(11));
                    movie.setTituloOriginal(cursor.getString(12));
                    movie.setGenero(cursor.getString(13));
                    movie.setClasificacion(cursor.getString(14));
                    movie.setDuracion(cursor.getString(15));
                    movie.setDirector(cursor.getString(16));
                    movie.setActores(cursor.getString(17));
                    movie.setSinopsis(cursor.getString(18));
                    movie.setImagenCartel(cursor.getString(19));
                    movie.setSello(cursor.getString(20));
                    movie.setSelloEstatus(cursor.getString(21));
                    movie.setClave(cursor.getString(22));
                    billboard.setPelicula(movie);
                    billboard.setIdComplejoVista(cursor.getInt(1));
                    billboard.setIdComplejo(cursor.getInt(2));
                    billboard.setIdPeliculaVista(cursor.getInt(3));
                    billboard.setIdShowtime(cursor.getInt(4));
                    billboard.setHorario(cursor.getString(5));
                    billboard.setFecha(cursor.getString(6));
                    billboard.setSello(cursor.getString(7));
                    billboard.setSala(cursor.getInt(8));
                    billboard.setId(cursor.getInt(9));
                    billboards.add(billboard);
                } while (cursor.moveToNext());

                cursor.close();
            }
        }
        return billboards;
    }

    public static List<Billboard> getBillboardsByPlaceId(Context context, String placeId) {
        SQLiteDatabase db = getSqLiteDatabase(context);
        List<Billboard> billboards = new ArrayList<>();
        if (db != null) {
            String[] args = new String[]{placeId};
            Cursor cursor = db.rawQuery("SELECT * FROM Cartelera AS c JOIN Pelicula AS p ON c.IdPelicula=p.Id WHERE c.IdComplejo=?", args);
            if (cursor.moveToFirst()) {
                do {
                    Billboard billboard = new Billboard();
                    Movie movie = new Movie();
                    movie.setId(cursor.getInt(0));
                    movie.setTitulo(cursor.getString(11));
                    movie.setTituloOriginal(cursor.getString(12));
                    movie.setGenero(cursor.getString(13));
                    movie.setClasificacion(cursor.getString(14));
                    movie.setDuracion(cursor.getString(15));
                    movie.setDirector(cursor.getString(16));
                    movie.setActores(cursor.getString(17));
                    movie.setSinopsis(cursor.getString(18));
                    movie.setImagenCartel(cursor.getString(19));
                    movie.setSello(cursor.getString(20));
                    movie.setSelloEstatus(cursor.getString(21));
                    movie.setClave(cursor.getString(22));
                    billboard.setPelicula(movie);
                    billboard.setIdComplejoVista(cursor.getInt(1));
                    billboard.setIdComplejo(cursor.getInt(2));
                    billboard.setIdPeliculaVista(cursor.getInt(3));
                    billboard.setIdShowtime(cursor.getInt(4));
                    billboard.setHorario(cursor.getString(5));
                    billboard.setFecha(cursor.getString(6));
                    billboard.setSello(cursor.getString(7));
                    billboard.setSala(cursor.getInt(8));
                    billboard.setId(cursor.getInt(9));
                    billboards.add(billboard);
                } while (cursor.moveToNext());

                cursor.close();
            }
        }
        return billboards;
    }

    public static List<String> getImagesByMovieId(Context context, String movieId) {
        SQLiteDatabase db = getSqLiteDatabase(context);
        List<String> images = new ArrayList<>();
        if (db != null) {
            String[] args = new String[]{movieId};
            Cursor cursor = db.rawQuery("SELECT m.* FROM Multimedia AS m  WHERE m.IdPelicula=?", args);
            if (cursor.moveToFirst()) {
                do {
                    images.add(Config.API_IMAGE_URL + cursor.getString(1));
                } while (cursor.moveToNext());

                cursor.close();
            }
        }
        return images;
    }
}
