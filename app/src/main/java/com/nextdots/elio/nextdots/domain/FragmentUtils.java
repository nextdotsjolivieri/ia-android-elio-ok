package com.nextdots.elio.nextdots.domain;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


public class FragmentUtils {
    public static void setFragmentContent(FragmentActivity context, int content,
                                          Fragment fragment, Bundle arguments) {
        FragmentManager fragmentManager = context.getSupportFragmentManager();
        if (arguments != null) {
            fragment.setArguments(arguments);
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(content, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
