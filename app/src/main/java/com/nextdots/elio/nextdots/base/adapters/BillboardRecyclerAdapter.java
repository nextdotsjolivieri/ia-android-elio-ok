package com.nextdots.elio.nextdots.base.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.domain.ShareSocialNetworks;

import java.util.List;

public class BillboardRecyclerAdapter extends RecyclerView.Adapter<BillboardRecyclerAdapter.CityViewHolder> {

    private List<Billboard> billboards;
    private FragmentActivity context;
    private boolean shareBillboard;

    public BillboardRecyclerAdapter(FragmentActivity context, @Nullable List<Billboard> billboards) {
        this.billboards = billboards;
        this.context = context;
    }

    public BillboardRecyclerAdapter(FragmentActivity context, @Nullable List<Billboard> billboards, boolean shareBillboard) {
        this.billboards = billboards;
        this.context = context;
        this.shareBillboard = shareBillboard;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflatedView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_billboard, viewGroup, false);
        return new CityViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        Billboard billboard = this.billboards.get(position);
        holder.txtTitle.setText(billboard.getPelicula().getTitulo());
        holder.txtTitleOriginal.setText(billboard.getPelicula().getTituloOriginal());
        holder.txtGenre.setText(String.format("Genero: %s", billboard.getPelicula().getGenero()));
        holder.txtClasification.setText(String.format("Clasificación: %s", billboard.getPelicula().getClasificacion()));
        holder.txtDuration.setText(String.format("Duración: %s", billboard.getPelicula().getDuracion()));
        holder.txtDate.setText(String.format("Fecha: %s", billboard.getFecha()));
        holder.txtSchedule.setText(String.format("Horario: %s", billboard.getHorario()));
        holder.txtRoom.setText(String.format("Sala: %s", billboard.getSala()));
        if (shareBillboard) {
            holder.imgBtnShare.setVisibility(View.VISIBLE);
            holder.imgBtnShare.setOnClickListener(v -> {
                String body = billboard.getPelicula().getTitulo() +
                        " podrá disfrutarla en la sala " + billboard.getSala() +
                        " a las " + billboard.getHorario() +
                        " el dia de hoy " + billboard.getFecha() +
                        ". Ven a visitarnos en www.cinepolis.com.";
                ShareSocialNetworks.shareText(context, body);
            });
        } else {
            holder.imgBtnShare.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return billboards.size();
    }

    public Billboard getItem(int index) {
        return billboards.get(index);
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtTitleOriginal;
        TextView txtGenre;
        TextView txtClasification;
        TextView txtDuration;
        TextView txtDate;
        TextView txtSchedule;
        TextView txtRoom;
        ImageButton imgBtnShare;

        CityViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtTitleOriginal = (TextView) itemView.findViewById(R.id.txtTitleOriginal);
            txtGenre = (TextView) itemView.findViewById(R.id.txtGenre);
            txtClasification = (TextView) itemView.findViewById(R.id.txtClasification);
            txtDuration = (TextView) itemView.findViewById(R.id.txtDuration);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtSchedule = (TextView) itemView.findViewById(R.id.txtSchedule);
            txtRoom = (TextView) itemView.findViewById(R.id.txtRoom);
            imgBtnShare = (ImageButton) itemView.findViewById(R.id.imgBtnShare);
        }
    }
}
