package com.nextdots.elio.nextdots.base;


import com.nextdots.elio.nextdots.base.UI.TabsView;
import com.nextdots.elio.nextdots.base.events.BaseEvent;

public interface TabsPresenter {
    void onCreate();

    void onDestroy();

    void onListBillboards(String movieId);

    void onListGalleryImages(String movieId);

    void onEventMainThread(BaseEvent event);

    TabsView getView();
}
