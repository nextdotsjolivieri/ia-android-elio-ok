package com.nextdots.elio.nextdots.base.UI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nextdots.elio.nextdots.CinepolisApp;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.BasePresenter;
import com.nextdots.elio.nextdots.base.DI.BaseComponent;
import com.nextdots.elio.nextdots.base.adapters.PlaceRecyclerAdapter;
import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.base.entities.Place;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.domain.FragmentUtils;
import com.nextdots.elio.nextdots.domain.RecyclerItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlacesFragment extends Fragment implements BaseView, RecyclerItemClickListener.OnItemClickListener {

    private BasePresenter presenter;
    @BindView(R.id.placesRecyclerView)
    RecyclerView placesRecyclerView;
    private ProgressDialog progressDialog;
    private PlaceRecyclerAdapter adapter;

    public PlacesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_places, container, false);
        ButterKnife.bind(this, view);

        setupInjection();

        placesRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        placesRecyclerView.setLayoutManager(linearLayoutManager);
        placesRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), this)
        );
        presenter.onCreate();

        Bundle args = getArguments();
        if (args != null) {
            String cityId = args.getString(Config.KEY_CITY);
            presenter.onListPlaces(cityId);
        }

        return view;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading_places));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onListPlaces(List<Place> places) {
        if (places != null) {
            adapter = new PlaceRecyclerAdapter(getActivity(), places);
            placesRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onListBillboard(List<Billboard> billboards) {
    }

    public void setupInjection() {
        CinepolisApp app = new CinepolisApp();
        BaseComponent productComponent = app.getBaseComponent(getActivity(), this);
        presenter = productComponent.getBasePresenter();
    }

    @Override
    public void onItemClick(View childView, int position) {
        Place place = adapter.getItem(position);
        Fragment fragment = new BillboardsFragment();
        Bundle args = new Bundle();
        args.putSerializable(Config.KEY_PLACE, place);
        FragmentUtils.setFragmentContent(getActivity(), R.id.fragment_content, fragment, args);
    }

    @Override
    public void onItemLongPress(View childView, int position) {

    }
}