package com.nextdots.elio.nextdots.base.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.adapters.ScreenSlidePagerAdapter;
import com.nextdots.elio.nextdots.domain.Config;

import java.util.ArrayList;

public class ScreenSlidePagerActivity extends AppCompatActivity {
    private ViewPager pager;
    private ArrayList<String> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);

        Intent intent = getIntent();
        if (intent != null) {
            images = intent.getStringArrayListExtra(Config.KEY_LIST_IMAGES);
        }

        pager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), images);
        pager.setAdapter(mPagerAdapter);
    }
}
