package com.nextdots.elio.nextdots.base.UI;

import com.nextdots.elio.nextdots.base.entities.Billboard;

import java.util.List;

public interface TabsView {
    void showProgress();

    void hideProgress();

    void onError(String error);

    void onListBillboards(List<Billboard> billboards);

    void onListGalleryImages(List<String> images);
}
