package com.nextdots.elio.nextdots.domain;

import android.content.Context;
import android.content.Intent;

import com.nextdots.elio.nextdots.R;

public class ShareSocialNetworks {

    public static void shareText(Context context, String body) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string.share_title_social_networks)));
    }
}
