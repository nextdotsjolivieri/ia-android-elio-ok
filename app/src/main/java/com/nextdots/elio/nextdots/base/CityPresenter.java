package com.nextdots.elio.nextdots.base;


import com.nextdots.elio.nextdots.base.UI.CityView;
import com.nextdots.elio.nextdots.base.events.BaseEvent;

public interface CityPresenter {
    void onCreate();

    void onDestroy();

    void onListCities();

    void onEventMainThread(BaseEvent event);

    CityView getView();
}
