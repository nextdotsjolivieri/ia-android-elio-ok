package com.nextdots.elio.nextdots.base.DI;


import com.nextdots.elio.nextdots.base.BaseInteractor;
import com.nextdots.elio.nextdots.base.BasePresenter;
import com.nextdots.elio.nextdots.base.BasePresenterImpl;
import com.nextdots.elio.nextdots.base.UI.BaseView;
import com.nextdots.elio.nextdots.lib.base.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class BaseModule {
    private BaseView baseView;

    public BaseModule(BaseView baseView) {
        this.baseView = baseView;
    }

    @Provides
    @Singleton
    BaseView providesPlaceView() {
        return this.baseView;
    }

    @Provides
    @Singleton
    BasePresenter providesBasePresenter(EventBus eventBus,
                                         BaseView baseView,
                                         BaseInteractor baseInteractor) {
        return new BasePresenterImpl(eventBus, baseView, baseInteractor);
    }
}
