package com.nextdots.elio.nextdots.domain;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferencesUtil {
    private String TOKEN_KEY = "TOKEN_KEY";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public SharePreferencesUtil(Context context) {
        String PREF_FILE = "PREF_FILE";
        preferences = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
    }

    public String getCityId() {
        return preferences.getString(Config.KEY_CITY, "");
    }

    public void saveCityId(String cityId) {
        editor = preferences.edit();
        editor.putString(Config.KEY_CITY, cityId);
        editor.apply();
    }
}
