package com.nextdots.elio.nextdots.domain;

public class Config {
    public static String API_URL = "http://api.cinepolis.com.mx/Consumo.svc/json/";
    public static String API_IMAGE_URL = "http://www.cinepolis.com/_MOVIL/Android/galeria/thumb/";
    public static String API_VIDEO_URL = "http://videos.cinepolis.com/";
    public static String APP_TAG = "CINEPOLIS_APP";
    public static String KEY_CITY = "KEY_CITY";
    public static String KEY_PLACE = "KEY_PLACE";
    public static String KEY_BILLBOARD = "KEY_BILLBOARD";
    public static String KEY_LIST_IMAGES = "KEY_LIST_IMAGES";
    public static String DATABASE_NAME = "cinepolis-db";
}
