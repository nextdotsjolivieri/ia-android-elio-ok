package com.nextdots.elio.nextdots.base.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.entities.Place;
import com.nextdots.elio.nextdots.domain.Config;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PlaceDetailsFragment extends Fragment {

    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtPhoneNumber)
    TextView txtPhoneNumber;
    @BindView(R.id.chkIsAdvancePurchase)
    CheckBox chkIsAdvancePurchase;
    @BindView(R.id.chkIsReservationAllowed)
    CheckBox chkIsReservationAllowed;
    @BindView(R.id.checkBox3)
    CheckBox chkIsFoodSales;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place_details, container, false);
        ButterKnife.bind(this, view);

        Bundle args = getArguments();
        if (args != null) {
            Place place = (Place) args.getSerializable(Config.KEY_PLACE);
            if (place != null) {
                txtName.setText(place.getNombre());
                txtAddress.setText(place.getDireccion());
                txtPhoneNumber.setText(place.getTelefono());
                chkIsAdvancePurchase.setChecked(place.isEsCompraAnticipada());
                chkIsReservationAllowed.setChecked(place.isEsReservaPermitida());
                chkIsFoodSales.setChecked(place.isEsVentaAlimentos());
            }
        }

        return view;
    }
}