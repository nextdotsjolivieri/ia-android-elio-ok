package com.nextdots.elio.nextdots.base.UI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nextdots.elio.nextdots.CinepolisApp;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.CityPresenter;
import com.nextdots.elio.nextdots.base.DI.BaseComponent;
import com.nextdots.elio.nextdots.base.adapters.CityRecyclerAdapter;
import com.nextdots.elio.nextdots.base.entities.City;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.domain.FragmentUtils;
import com.nextdots.elio.nextdots.domain.RecyclerItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CitiesFragment extends Fragment implements CityView, RecyclerItemClickListener.OnItemClickListener {

    private CityPresenter presenter;
    @BindView(R.id.citiesRecyclerView)
    RecyclerView citiesRecyclerView;
    private ProgressDialog progressDialog;
    private CityRecyclerAdapter adapter;

    public CitiesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_cities, container, false);
        ButterKnife.bind(this, view);

        setupInjection();

        citiesRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        citiesRecyclerView.setLayoutManager(linearLayoutManager);
        citiesRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), this)
        );

        presenter.onCreate();
        presenter.onListCities();
        return view;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading_cities));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onListCities(List<City> cities) {
        if (cities != null) {
            adapter = new CityRecyclerAdapter(getActivity(), cities);
            citiesRecyclerView.setAdapter(adapter);
        }
    }

    public void setupInjection() {
        CinepolisApp app = new CinepolisApp();
        BaseComponent productComponent = app.getBaseComponent(getActivity(), this);
        presenter = productComponent.getCityPresenter();
    }

    @Override
    public void onItemClick(View childView, int position) {
        final City obj = adapter.getItem(position);
        Fragment fragment = new PlacesFragment();
        Bundle args = new Bundle();
        args.putString(Config.KEY_CITY, String.valueOf(obj.getId()));
        FragmentUtils.setFragmentContent(getActivity(), R.id.fragment_content, fragment, args);
    }

    @Override
    public void onItemLongPress(View childView, int position) {
    }
}