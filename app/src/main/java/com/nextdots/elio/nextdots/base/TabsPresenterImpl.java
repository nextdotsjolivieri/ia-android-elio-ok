package com.nextdots.elio.nextdots.base;


import com.nextdots.elio.nextdots.base.UI.TabsView;
import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.base.events.BaseEvent;
import com.nextdots.elio.nextdots.lib.base.EventBus;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class TabsPresenterImpl implements TabsPresenter {

    private EventBus eventBus;
    private TabsView tabsView;
    private BaseInteractor baseInteractor;

    public TabsPresenterImpl(EventBus eventBus, TabsView tabsView, BaseInteractor baseInteractor) {
        this.eventBus = eventBus;
        this.tabsView = tabsView;
        this.baseInteractor = baseInteractor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        tabsView = null;
        eventBus.unregister(this);
    }

    @Override
    public void onListBillboards(String movieId) {
        tabsView.showProgress();
        baseInteractor.onListBillboards(movieId);
    }

    @Override
    public void onListGalleryImages(String movieId) {
        tabsView.showProgress();
        baseInteractor.onListGalleryImages(movieId);
    }

    @Override
    @Subscribe
    public void onEventMainThread(BaseEvent event) {
        switch (event.getType()) {
            case BaseEvent.onListBillboardsSuccess:
                onListBillboardsSuccess(event.getBillboards());
                break;
            case BaseEvent.onListGalleryImagesSuccess:
                onListGalleryImagesSuccess(event.getImages());
                break;
            case BaseEvent.onError:
                onDownloadError(event.getError());
                break;
        }
    }

    private void onDownloadError(String error) {
        if (tabsView != null) {
            tabsView.hideProgress();
            tabsView.onError(error);
        }
    }

    private void onListBillboardsSuccess(List<Billboard> billboards) {
        if (tabsView != null) {
            tabsView.hideProgress();
            tabsView.onListBillboards(billboards);
        }
    }

    private void onListGalleryImagesSuccess(List<String> images) {
        if (tabsView != null) {
            tabsView.hideProgress();
            tabsView.onListGalleryImages(images);
        }
    }

    @Override
    public TabsView getView() {
        return tabsView;
    }
}
