package com.nextdots.elio.nextdots.base.UI;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nextdots.elio.nextdots.CinepolisApp;
import com.nextdots.elio.nextdots.R;
import com.nextdots.elio.nextdots.base.DI.BaseComponent;
import com.nextdots.elio.nextdots.base.TabsPresenter;
import com.nextdots.elio.nextdots.base.adapters.GalleryRecyclerAdapter;
import com.nextdots.elio.nextdots.base.entities.Billboard;
import com.nextdots.elio.nextdots.domain.Config;
import com.nextdots.elio.nextdots.domain.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class GalleryTabFragment extends Fragment implements TabsView, RecyclerItemClickListener.OnItemClickListener {

    @BindView(R.id.galleryRecycleView)
    RecyclerView galleryRecycleView;
    private TabsPresenter presenter;
    private ProgressDialog progressDialog;
    private ArrayList<String> images = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, view);

        setupInjection();

        presenter.onCreate();

        Bundle args = getArguments();
        if (args != null) {
            Billboard billboard = (Billboard) args.getSerializable(Config.KEY_BILLBOARD);
            if (billboard != null) {
                presenter.onListGalleryImages(String.valueOf(billboard.getPelicula().getId()));
            }
        }

        galleryRecycleView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        galleryRecycleView.setLayoutManager(gridLayoutManager);
        galleryRecycleView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), this)
        );

        return view;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading_cities));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onListBillboards(List<Billboard> billboards) {
    }

    @Override
    public void onListGalleryImages(List<String> images) {
        if (images != null) {
            this.images = (ArrayList<String>) images;
            GalleryRecyclerAdapter adapter = new GalleryRecyclerAdapter(getActivity(), images);
            galleryRecycleView.setAdapter(adapter);
        }
    }

    public static GalleryTabFragment newInstance(Bundle args) {
        GalleryTabFragment fragment = new GalleryTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setupInjection() {
        CinepolisApp app = new CinepolisApp();
        BaseComponent productComponent = app.getBaseComponent(getActivity(), this);
        presenter = productComponent.getTabsPresenter();
    }

    @Override
    public void onItemClick(View childView, int position) {
        Intent intent = new Intent(getActivity(), ScreenSlidePagerActivity.class);
        intent.putStringArrayListExtra(Config.KEY_LIST_IMAGES, images);
        startActivity(intent);
    }

    @Override
    public void onItemLongPress(View childView, int position) {
    }
}